#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define LINESIZE 255

void search_str(FILE *file, char *search, int insensitive);
void toLower(char* s);

int main(int argc, char *argv[])
{

    char *str;
    char *filename;
    FILE *fp;
    int insensitive = 0;
    int numArgs = 3;

    if (argv[1] && strcmp(argv[1], "-i") == 0)
    {
        insensitive = 1;
        numArgs++;
    }

    if (argc < numArgs)
    {
        printf("please supply all arguments...");
        return 1;
    }
    else
    {
        str = argv[numArgs-1];
        filename = argv[numArgs-2];
    }

    fp = fopen(filename, "r");
    if (fp != NULL)
    {
        search_str(fp, str, insensitive);
        fclose(fp);
    }
    else
    {
        printf("file '%s' does not exist...", filename);
        return 1;
    }
}

void search_str(FILE *file, char *search, int insensitive)
{
    char line[LINESIZE];
    char linecmp[LINESIZE];
    while(fgets(line, LINESIZE, file) != NULL)
    {
        strcpy(linecmp, line);
        if (insensitive)
        {
            toLower(search);
            toLower(linecmp);
        }

        if (strstr(linecmp, search) != NULL)
        {
            printf("%s", line);
        }
    }
}

void toLower(char* s)
{
    for(char *p=s; *p; p++) *p=tolower(*p);
}
