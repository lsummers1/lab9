#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "md5.h"

int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        printf("error: please supply all arguments...");
        return 1;
    }

    FILE *src = fopen(argv[1], "r");
    if (src == NULL)
    {
        printf("error: file '%s' does not exist...", argv[1]);
        return 1;
    }

    FILE *dest = fopen(argv[2], "w");

    char line[255];
    while(1)
    {
        if (fgets(line, 255, src) == NULL)
        {
            break;
        }
        *strchr(line, '\n') = '\0';
        char *hash = md5(line, strlen(line));
        fputs(hash, dest);
        fputs("\n", dest);
        free(hash);

    }
    fclose(src);
    fclose(dest);
}
